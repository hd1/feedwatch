package us.d8u.feedwatch;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.swing.JOptionPane;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import com.apple.mrj.MRJFileUtils;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndEntryImpl;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;

import edu.stanford.ejalbert.exception.BrowserLaunchingInitializingException;

@SuppressWarnings("restriction")
public class Main implements Runnable {
	private static Method openURL;

	@SuppressWarnings({ "unchecked" })
	public static void initialize() throws BrowserLaunchingInitializingException {
		logger.debug("entered initialize");
		try {
			Class<MRJFileUtils> mrjFileUtilsClass = (Class<MRJFileUtils>) Class.forName("com.apple.mrj.MRJFileUtils");
			openURL = mrjFileUtilsClass.getDeclaredMethod("openURL", new Class[] { String.class });
		} catch (Exception e) {
			throw new BrowserLaunchingInitializingException(e);
		}
	}

	private static final long WAIT_TIME = 30 * 1000;
	private static Logger logger = Logger.getLogger(Main.class);
	static int latestHash = 0;

	@SuppressWarnings({ "unchecked" })
	public static void main(String[] args) {
		try {
			initialize();
		} catch (BrowserLaunchingInitializingException e2) {
			logger.fatal(e2.getMessage(), e2);
		}
		URL url = null;
		try {
			url = new URL(args[0]);
		} catch (Exception e2) {
			logger.fatal("defaulting to the stackoverflow front page, if you want to monitor something different, send a valid URL as the first parameter");
			try {
				url = new URL("http://stackoverflow.com/feeds");
			} catch (MalformedURLException e1) {
				logger.fatal(e1.getMessage(), e1);
			}
		}
		while (true) {
			DateTime currentRunStart = new DateTime();
			HttpURLConnection conn = null;
			try {
				conn = (HttpURLConnection) url.openConnection();
			} catch (IOException e) {
				logger.fatal(e.getMessage(), e);
			}
			SyndFeedInput input = new SyndFeedInput();
			SyndFeed feed = null;
			try {
				feed = input.build(new XmlReader(conn.getInputStream(), true));
			} catch (Throwable e) {
				logger.fatal(e.getMessage(), e);
				try {
					Thread.sleep(WAIT_TIME);
				} catch (InterruptedException e1) {
					continue;
				}
				continue;
			}

			List<SyndEntryImpl> entries = null;
			try {
				entries = feed.getEntries();
			} catch (NullPointerException e) {
				try {
					Thread.sleep(WAIT_TIME);
				} catch (InterruptedException e1) {
					logger.fatal(e1.getMessage(), e1);
					continue;
				}
			}
			SyndEntry latestEntry = entries.get(1);
			if (latestHash == 0) latestHash = latestEntry.hashCode();
			if (latestHash != latestEntry.hashCode()) {
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				ObjectOutputStream oos = null;
				try {
					oos = new ObjectOutputStream(baos);
				} catch (IOException e1) {
					logger.fatal(e1.getMessage(), e1);
				}
				try {
					oos.writeObject(latestEntry);
				} catch (IOException e) {
					logger.fatal(e.getMessage(), e);
				}
				try {
					oos.writeObject(currentRunStart);
				} catch (IOException e) {
					logger.fatal(e.getMessage(), e);
				}
				System.setProperty("passableReadOnlyData", baos.toString());
				new Thread(new Main()).start();
			}
		}
	}

	@Override
	public void run() {
		{
			ByteArrayInputStream bais = new ByteArrayInputStream(System.getProperty("passableReadOnlyData").getBytes());
			ObjectInputStream ois = null;
			try {
				ois = new ObjectInputStream(bais);
			} catch (IOException e1) {
				logger.fatal(e1.getMessage(), e1);
			}
			DateTime currentRunStart = null;
			try {
				currentRunStart = (DateTime) ois.readObject();
			} catch (ClassNotFoundException e1) {
				logger.fatal(e1.getMessage(), e1);
			} catch (IOException e1) {
				logger.fatal(e1.getMessage(), e1);
			}
			SyndEntryImpl latestEntry = null;
			try {
				latestEntry = (SyndEntryImpl) ois.readObject();
			} catch (ClassNotFoundException e1) {
				logger.fatal(e1.getMessage(), e1);
			} catch (IOException e1) {
				logger.fatal(e1.getMessage(), e1);
			}
			int interested = JOptionPane.showOptionDialog(null, latestEntry.getDescription().getValue().replaceAll("<[^>]+>", "").replaceAll("&lt;", "<")
					.replaceAll("&gt;", ">").replaceAll("&amp;", "&"), latestEntry.getTitle(), JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE,
					null, new String[] { "View", "Skip" }, "View");
			if (interested == JOptionPane.YES_OPTION) {
				logger.debug(latestEntry.getLink());
				try {
					openURL.invoke(null, latestEntry.getLink());
				} catch (IllegalAccessException e) {
					logger.fatal(e.getMessage(), e);
				} catch (IllegalArgumentException e) {
					logger.fatal(e.getMessage(), e);
				} catch (InvocationTargetException e) {
					logger.fatal(e.getMessage(), e);
				}
			}
			latestHash = latestEntry.hashCode();
			logger.debug("Sleeping");
			while ((new DateTime().getMillis() - currentRunStart.getMillis()) < WAIT_TIME) {
			}
		}
	}
}
